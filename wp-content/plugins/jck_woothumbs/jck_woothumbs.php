<?php
/*
Plugin Name: WooThumbs
Plugin URI: http://codecanyon.net/item/multiple-images-per-variation-for-woocommerce/2867927?ref=jamesckemp
Description: Display multiple images for each variation of a product.
Author: James Kemp
Version: 3.0.3
Author URI: http://www.jckemp.com/
*/

class jck_wt
{
	private $pluginName = 'jck_wt';
	private $pluginFullName = 'WooThumbs';
	private $version = '3.0.2';
	
	// Set up the default options
	private $jck_wt_options = array(
		'layout' => 'Lightbox'
	);
	
	// Remove the default images from WooCommerce Product Pages
	public function remove_hooks() {
		remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
		// Remove images from Bazar theme
		if( class_exists( 'YITH_WCMG' ) ) {
			$this->remove_filters_for_anonymous_class( 'woocommerce_before_single_product_summary', 'YITH_WCMG_Frontend', 'show_product_images', 20 );
			$this->remove_filters_for_anonymous_class( 'woocommerce_product_thumbnails', 'YITH_WCMG_Frontend', 'show_product_thumbnails', 20 );
		}
	}
	
	// Allow to remove method for a hook when it's a class method used and the class doesn't have a variable assigned, but the class name is known
	public function remove_filters_for_anonymous_class( $hook_name = '', $class_name ='', $method_name = '', $priority = 0 ) {
	        global $wp_filter;
	        
	        // Take only filters on right hook name and priority
	        if ( !isset($wp_filter[$hook_name][$priority]) || !is_array($wp_filter[$hook_name][$priority]) )
	                return false;
	        
	        // Loop on filters registered
	        foreach( (array) $wp_filter[$hook_name][$priority] as $unique_id => $filter_array ) {
	                // Test if filter is an array ! (always for class/method)
	                if ( isset($filter_array['function']) && is_array($filter_array['function']) ) {
	                        // Test if object is a class, class and method is equal to param !
	                        if ( is_object($filter_array['function'][0]) && get_class($filter_array['function'][0]) && get_class($filter_array['function'][0]) == $class_name && $filter_array['function'][1] == $method_name ) {
	                                unset($wp_filter[$hook_name][$priority][$unique_id]);
	                        }
	                }
	                
	        }
	        
	        return false;
	}
  
	public function frontend_scripts() {
		
		if(is_product()){
			global $post;
			
			$settings = get_option( 'jck_wt_options', $this->jck_wt_options );
		
			if($settings['layout'] == 'Lightbox'){
				wp_enqueue_script( 'prettyPhoto' );
				wp_enqueue_script( 'prettyPhoto-init' );
				wp_enqueue_style( 'woocommerce_prettyPhoto_css' );
			}
			
			if($settings['layout'] == 'Slides'){
				wp_enqueue_script('responsiveslides', plugins_url('js/responsiveslides.min.js', __FILE__), array('jquery'), '1.54', true);
				wp_enqueue_style('responsiveslides', plugins_url('css/rslides.css', __FILE__), false, '1.54');
			}
			
			if($settings['layout'] == 'Zoom'){
				wp_enqueue_script('elevatezoom', plugins_url('js/elevatezoom.js', __FILE__), array('jquery'), '3.0.6', true);
			}
		
			wp_enqueue_script('imagesloaded', plugins_url('js/imagesloaded.js', __FILE__), array('jquery'), '3.0.4', true);
			wp_enqueue_script($this->pluginName, plugins_url('js/scripts.js', __FILE__), array('jquery'), '2.0.1', true);
			
			$settings = get_option( 'jck_wt_options', $this->jck_wt_options );
			
			$product = get_product( $post->ID );
			
			$vars = array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'nonce' => wp_create_nonce( $this->pluginName.'_ajax' ),
				'layout' => $settings['layout'],
				'product_id' => $post->ID,
				'loading_icon' => plugins_url('img/loading.gif', __FILE__),
				'product_type' => $product->product_type
			);
			wp_localize_script( $this->pluginName, 'vars', $vars );
			
			// Dequeue Scripts
			wp_dequeue_script('yith_wcmg_frontend'); // Bazar Theme
			
			// Enqueue Styles			
			wp_enqueue_style($this->pluginName, plugins_url('css/styles.css', __FILE__), false, '1.0.0');
		}
	}
	
	public function admin_scripts() {
		global $post, $pagenow;
		if($post) {
			if(get_post_type( $post->ID ) == "product" && ($pagenow == "post.php" || $pagenow == "post-new.php")) {	
				wp_enqueue_script($this->pluginName, plugins_url('js/admin-scripts.js', __FILE__), array('jquery'), '2.0.1', true);
				wp_enqueue_style( 'jck_wt_admin_css', plugins_url('css/admin-styles.css', __FILE__), false, '2.0.1' );
				
				$vars = array(
					'ajaxurl' => admin_url( 'admin-ajax.php' ),
					'nonce' => wp_create_nonce( $this->pluginName.'_ajax' )
				);
				wp_localize_script( $this->pluginName, 'vars', $vars );
			}
		}
	}
	
	function save_images($post_id){
	
		if(isset($_POST['variation_image_gallery'])) {
			foreach($_POST['variation_image_gallery'] as $varID => $variation_image_gallery) {
				update_post_meta($varID, 'variation_image_gallery', $variation_image_gallery);	
			}
		}
		
	}
	
/* 	=============================
   	Frontend Functions 
   	============================= */
	
	public function display_images(){
		global $post, $woocommerce, $product;
		
		$single_img_size = get_option('shop_single_image_size', array('width' => 300, 'height' => 300));
		
		echo '<div id="jck_wt_images" style="width: '.$single_img_size['width'].'px">';
			echo '<div class="jck_wt_layout"></div>';
		echo '</div>';
		
		// Thumbnails are displayed here
	}
	
	/* AJAX Image request */

	function load_images() {
		
		if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], $this->pluginName.'_ajax' ) ) { die ( 'Invalid Nonce' ); }
			
			header('Content-type: application/json');
			
			$variation = array();
			
			$variation['id'] = $_REQUEST['variation'];
			
			if($_REQUEST['reset'] == 'true') {
				$default_product = get_product($_REQUEST['variation']);
				$attachments = $default_product->get_gallery_attachment_ids();		
			} else {
				$attachments = get_post_meta($_REQUEST['variation'], 'variation_image_gallery', true);
				$attachments = array_filter(explode(',', $attachments));
			}
			
			$variation['thumbnails'] = $this->thumbnails($attachments);
			
			$variation['main_image'] = $this->main_image($_REQUEST['variation'], $_REQUEST['main_product_id']);			
			
			echo json_encode($variation);
		
		exit;
	}
	
	function thumbnails($attachments){
		if(count($attachments) > 0 && is_array($attachments)) {
			$thumbnails = array();
			$i = 0; foreach($attachments as $attachment){
				$imageLrg =  wp_get_attachment_image_src($attachment, 'full' );
				$imageSingle =  wp_get_attachment_image_src($attachment, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ) );
				$imageCatalog =  wp_get_attachment_image_src($attachment, apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' ) );
				$thumbnails[$i]['single'] = wp_get_attachment_image($attachment, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ));
				$thumbnails[$i]['large_src'] = $imageLrg[0];
				$thumbnails[$i]['single_src'] = $imageSingle[0];
				$thumbnails[$i]['catalog_src'] = $imageCatalog[0];
			$i++; }
			return $thumbnails;
		} else {
			return array();
		}
	}
	
	function main_image($varid, $mainProdId){
		if ( has_post_thumbnail($varid) ) {
			
			$post_thumbnail_id = get_post_thumbnail_id( $varid );
			$imageLrg =  wp_get_attachment_image_src($post_thumbnail_id, 'full' );
			$imageSingle =  wp_get_attachment_image_src($post_thumbnail_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ) );
			$imageCatalog =  wp_get_attachment_image_src($post_thumbnail_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' ) );
			return array(
				'single' => get_the_post_thumbnail( $varid, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) ),
				'large_src' => $imageLrg[0],
				'single_src' => $imageSingle[0],
				'catalog_src' => $imageCatalog[0]
			);

		} else {
			// Check if we're currently looking for default images for the main product.
			if($mainProdId == $varid) {
				// If there is no image for the variation OR main product, show the placeholder
				$placeholder = apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="Placeholder" />', woocommerce_placeholder_img_src() ), $varid );
				
				return array(
					'single' => $placeholder,
					'large_src' => woocommerce_placeholder_img_src(),
					'single_src' => woocommerce_placeholder_img_src(),
					'catalog_src' => woocommerce_placeholder_img_src()
				);
			} else {
				// if we're looking at a variation, and it has no image assigned, get the image for the main product instead.
				return $this->main_image($mainProdId, 0);
			}

		}
	}

/* 	=============================
   	Admin Functions 
   	============================= */
   	
   	function admin_load_thumbnails() {
		
		if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], $this->pluginName.'_ajax' ) ) { die ( 'Invalid Nonce' ); }
			
			$attachments = get_post_meta($_GET['varID'], 'variation_image_gallery', true);
			$attachmentsExp = array_filter(explode(',', $attachments));
			$imgIDs = array(); ?>
			
			<ul class="wooThumbs">
			
				<?php if(!empty($attachmentsExp)) { ?>
				
					<?php foreach($attachmentsExp as $id) { $imgIDs[] = $id; ?>
						<li class="image" data-attachment_id="<?php echo $id; ?>">
							<a href="#" class="delete" title="Delete image"><?php echo wp_get_attachment_image( $id, 'thumbnail' ); ?></a>
						</li>
					<?php } ?>
				
				<?php } ?>
			
			</ul>
			<input type="hidden" class="variation_image_gallery" name="variation_image_gallery[<?php echo $_GET['varID']; ?>]" value="<?php echo $attachments; ?>">
		
		<?php exit;
	}
	
/* 	=============================
   	Settings Page 
   	============================= */
   
	function jck_wt_register_settings() {
	    register_setting( 'jck_wt_settings', 'jck_wt_options', array( &$this, 'jck_wt_validate_options' ) );
	}
	
	function jck_wt_options() {
	    add_submenu_page( 'woocommerce', $this->pluginFullName, $this->pluginFullName, 'activate_plugins', $this->pluginName, array( &$this, 'jck_wt_options_page') );
	}
	
	function jck_wt_options_page() {
	 
	    if ( ! isset( $_REQUEST['updated'] ) )
	    $_REQUEST['updated'] = false; // This checks whether the form has just been submitted. ?>
	 
	    <div class="wrap">
	 
	    <div id="icon-options-general" class="icon32"><br></div><?php echo "<h2>" . __( $this->pluginFullName . ' Options' ) . "</h2>";
	    // This shows the page's name and an icon if one has been provided ?>
	 
	    <?php if ( false !== $_REQUEST['updated'] ) : ?>
	    <div><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
	    <?php endif; // If the form has just been submitted, this shows the notification ?>
	 
	    <form method="post" action="options.php">
	 
		    <?php $settings = get_option( 'jck_wt_options', $this->jck_wt_options ); ?>
		 
		    <?php settings_fields( 'jck_wt_settings' );
		    /* This function outputs some hidden fields required by the form,
		    including a nonce, a unique number used to ensure the form has been submitted from the admin page
		    and not somewhere else, very important for security */ ?>
		 
		    <table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row">
							<label for="layout"><strong><?php _e('Layout',$this->pluginName); ?></strong></label>
						</th>
						<td>
							<select id="layout" name="jck_wt_options[layout]">
								<option value="Lightbox" <?php selected($settings['layout'], "Lightbox"); ?>>Lightbox</option>
								<option value="Slides" <?php selected($settings['layout'], "Slides"); ?>>Slides</option>
								<option value="Zoom" <?php selected($settings['layout'], "Zoom"); ?>>Zoom</option>
							</select>
							<p class="description"><?php _e('Choose a layout for your product imagery.',$this->pluginName); ?></p>
							</td>
					</tr>
				</tbody>
			</table>
		 
		    <p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="Save Changes"></p>
	 
	    </form>
	 
	    </div>
	 
	    <?php
	}
	
	function jck_wt_validate_options( $input ) {
	    $jck_wt_options = $this->jck_wt_options;
	 
	    $settings = get_option( 'jck_wt_options', $jck_wt_options );
	 
	    // We strip all tags from the text field, to avoid vulnerablilties like XSS
	    $input['layout'] = wp_filter_nohtml_kses( $input['layout'] );
	 
	    return $input;
	}
  
/* =============================
   PHP 5 Constructor. 
   ============================= */
	function __construct() {
		add_action('wp_enqueue_scripts', array(
			  &$this,
			  'frontend_scripts'
		),50); 
		
		add_action( 'admin_enqueue_scripts', array(
			  &$this,
			  'admin_scripts'
		)); 
		
		add_action( 'woocommerce_process_product_meta', array(
			&$this,
			'save_images'
		));
		
		// Frontend AJAX
		
		add_action( 'wp_ajax_nopriv_load_images', array(
			&$this,
			'load_images'
		));
		
		add_action( 'wp_ajax_load_images', array(
			&$this,
			'load_images'
		));
		
		// Admin Ajax
		
		add_action( 'wp_ajax_admin_load_thumbnails', array(
			&$this,
			'admin_load_thumbnails'
		));
		 
		// Activate Settings
		add_action( 'admin_init', array(
			  &$this,
			  'jck_wt_register_settings'
		));
		add_action( 'admin_menu', array(
			  &$this,
			  'jck_wt_options'
		));
		
		add_action( 'after_setup_theme', array( 
			&$this, 
			'remove_hooks' 
		));
		
		add_action( 'woocommerce_before_single_product_summary', array( 
			&$this, 
			'display_images' 
		), 20);
	}
	
/* =============================
   PHP 4 Compatible Constructor. 
   ============================= */
	function jck_wt() {
		$this->__construct();
	}
  
} // End jck_wt Class
$jck_wt = new jck_wt; // Start an instance of the plugin class