jQuery(document).ready(function($) {

/* 	=============================
   	Variables 
   	============================= */
	$variations_form = $('form.variations_form');
	$img_container = $('#jck_wt_images');
	$swatches = ($('#swatches-and-photos-css').length > 0) ? true : false;
	
	$fade_speed = 250;
	
	jck_wt_set_default_images();
	if(vars.product_type != "variable"){
		// If this isn't a variable product, trigger this so the default images load in
		jck_wt_reset_img_setup();
	}
	
/* 	=============================
   	Found Variation 
   	============================= */
   	
   	// This triggers a show_variations event, if it hasn't happened already and a default var is set for the product
   	
   	$initial_load = false;

   	$variations_form.on('change', function(){
	   	if($('input[name=variation_id]').val() != "" && !$initial_load){
	   		$chosen_var = [{'variation_id': $('input[name=variation_id]').val()}];
	   		$variations_form.trigger( 'show_variation', $chosen_var );
	   		$initial_load = true;
	   	}
   	});


	$variations_form.on( 'show_variation', function( event, variation ) {
		if(!$img_container.hasClass('loading')){
			// If swatches plugin is installed.
			if($swatches){
				$var_id = $('input[name=variation_id]').val();
			} else {
				$var_id = variation.variation_id;
			}
			
			// If the selected variation isn't the same as the one before, start loading it in
			if(!$img_container.hasClass('var-'+$var_id)){
				
				// Remove the variation class
				$img_container.removeClass(function (index, css) {
				    return (css.match (/\bvar-\S+/g) || []).join(' ');
				});
				$img_container.addClass('loading');
		
				$img_args = {
					'id': $var_id,
					'reset': false
				}
		
				jck_wt_get_images($img_args, function(data){
					$img_container.html(jck_wt_layout(data));
					$img_container.removeClass('reset');			
					jck_wt_fadein_layout_wrapper();
					$img_container.removeClass('loading');
					$img_container.addClass('var-'+$var_id);
					$initial_load = true;
				});
			
			}
		}
	   
	});
	
	if($swatches){
		if($('input[name=variation_id]').val() != ""){
			$variations_form.trigger('show_variation');
		}
	}
	
/* 	=============================
   	Reset Images 
   	============================= */
   	
   	// Forsome reason I had to set it up like this to work for both normal and the swatches plugin
   	if($swatches){
		$variations_form.on('reset_image', jck_wt_reset_img_setup());
	} else {
		$variations_form.on('reset_image', function(){
			jck_wt_reset_img_setup()
		});
	}
	
	// Make sure the default images are set, then trigger the fadeout/fadein
	function jck_wt_reset_img_setup(){
		if(!$img_container.hasClass('loading')){
			if(!$img_container.hasClass('reset')){
				// Remove the variation class
				$img_container.removeClass(function (index, css) {
				    return (css.match (/\bvar-\S+/g) || []).join(' ');
				});
				$img_container.addClass('loading');
				if(typeof $default_imgs == 'undefined'){
					// If the default images were not set in time, set theme here
					jck_wt_set_default_images(function(){					
						jck_wt_reset($default_imgs);
					});
				} else {
					// If there are already default images set, do what you need to here
					jck_wt_reset($default_imgs);
				}
			}
		}
	}
	
	// Reset images fadeout/fadein
	function jck_wt_reset(images){
		$img_container.addClass('reset');
		jck_wt_fadeout_layout_wrapper(function(){
			$img_container.html(jck_wt_layout(images));
			jck_wt_fadein_layout_wrapper();
			$img_container.removeClass('loading');
			$initial_load = true;
		});
	}

/* 	=============================
   	Functions 
   	============================= */

	function jck_wt_get_images(args, callback){
		
		$layout_wrapper = $('.jck_wt_layout');
		
		jck_wt_fadeout_layout_wrapper(function(){
					
			var ajaxargs = {
				'action': 			'load_images',
				'nonce':   			vars.nonce,
				'variation':		args.id,
				'reset':			args.reset,
				'main_product_id': 	vars.product_id
			}
		
			$.ajax({
				url: vars.ajaxurl,
				data: ajaxargs
			}).success(function(data) {
				if(callback != undefined) {
					callback(data);
				}
			});
		
		});
	}
	
	function jck_wt_set_default_images(callback){
		$default_imgs_args = {
			'id': vars.product_id,
			'reset': true
		}
		jck_wt_get_images($default_imgs_args, function(data){
			$default_imgs = data;
			if(callback != undefined) {
				callback(data);
			}
		});
	}
	
	function jck_wt_layout(images){
		layout = "";
		
		if(vars.layout == "Lightbox"){
			layout += '<div id="jck_wt_lightbox" class="jck_wt_layout" style="display:none;">';
				// If thumbnails are set, and there are more than 0, set the gallery attr in the rel
				$gallery = (typeof images['thumbnails'] != 'undefined') ? ($.map(images['thumbnails'], function(n, i) { return i; }).length > 0) ? "[product-gallery]" : "" : "";
				layout += '<div id="jck_wt_main_image"><a href="' + images['main_image'].large_src + '" itemprop="image" class="woocommerce-main-image jck_wt_lightbox" title="" rel="prettyPhoto'+$gallery+'">' + images['main_image'].single + '</a></div>';
				if(typeof images['thumbnails'] != 'undefined' && images['thumbnails'].length > 0){
					layout += '<ul id="jck_wt_thumbnails">';
						$.each(images['thumbnails'], function(key, val){
							layout += '<li class="jck_wt_thumbnail"><a href="'+val.large_src+'" class="jck_wt_lightbox" rel="prettyPhoto[product-gallery]">' + val.single + '</a></li>';
						});
					layout += '</ul>';
				}
			layout += '</div>';
		}
		
		if(vars.layout == "Slides"){
			layout += '<div id="jck_wt_slides" class="jck_wt_layout" style="display:none;">';
				layout += '<ul class="rslides">';
					layout += '<li>' + images['main_image'].single + '</li>';
					if(typeof images['thumbnails'] != 'undefined' && images['thumbnails'].length > 0){
						$.each(images['thumbnails'], function(key, val){
							layout += '<li>' + val.single + '</li>';
						});
					}
				layout += '</ul>';
			layout += '</div>';
		}
		
		if(vars.layout == "Zoom"){
			layout += '<div id="jck_wt_zoom" class="jck_wt_layout" style="display:none;">';
				
					layout += '<img id="jck_wt_zoom_img" src="' + images['main_image'].single_src + '" data-zoom-image="' + images['main_image'].large_src + '"/>';
					if(typeof images['thumbnails'] != 'undefined' && images['thumbnails'].length > 0){
						layout += '<ul id="jck_wt_zoom_gallery">';
							layout += '<li><a class="active" href="' + images['main_image'].large_src + '" data-image="' + images['main_image'].single_src + '" data-zoom-image="' + images['main_image'].large_src + '"><img src="' + images['main_image'].catalog_src + '" /></a></li>';
							$.each(images['thumbnails'], function(key, val){
								layout += '<li><a href="' + val.large_src + '" data-image="' + val.single_src + '" data-zoom-image="' + val.large_src + '"><img src="' + val.catalog_src + '" /></a></li>';
							});						
						layout += '</ul>';
					}
			layout += '</div>';
		}
		
		return layout;
	}
	
	function jck_wt_fadein_layout_wrapper(){
		$layout_wrapper = $('.jck_wt_layout');
			
		$layout_wrapper.css({'opacity':0,'display':'block'});
		
		$layout_wrapper.imagesLoaded( function() {
			$layout_wrapper.animate({'opacity':1}, $fade_speed);
			jck_wt_animate_layout_height();
			
			if(vars.layout == "Lightbox"){
				// if we're using the lightbox layout, then trigger it here
				// This is the default WooCommerce prettyPhoto trigger
				$("#jck_wt_lightbox a.jck_wt_lightbox").prettyPhoto({
					social_tools:!1,
					theme:"pp_woocommerce",
					horizontal_padding:40,
					opacity:.9
				});
			}
			if(vars.layout == "Slides"){
				// If we're using the slides layout, trigger them here
				$("#jck_wt_slides .rslides").responsiveSlides({
					'nav': true,
					'auto': true,
					'speed': 500,
					'timeout': 4000,
					'prevText': "&#10094;",
					'nextText': "&#10095;"
				});
			}
			if(vars.layout == "Zoom"){
				// If we're using the slides layout, trigger them here
				$('#jck_wt_zoom_img').elevateZoom({
					'gallery': 'jck_wt_zoom_gallery', 
					'cursor': 'pointer', 
					'galleryActiveClass': 'active', 
					'imageCrossfade': true, 
					'loadingIcon': vars.loading_icon
				});
			}
		});
	}
	
	function jck_wt_fadeout_layout_wrapper(callback){
		$img_container.css('height', $img_container.height() );
		$layout_wrapper.fadeOut($fade_speed, function(){
			if(callback != undefined) {
				callback();
			}
		});
	}
	
	function jck_wt_animate_layout_height(){
		$img_container.wrapInner('<div/>');
		var $newheight = $img_container.find('div:first').height();
		$img_container.animate({height: $newheight}, $fade_speed);
		$img_container.unwrapInner();
	}
	
	if(vars.layout == "Slides"){
		$('#jck_wt_images').on('mouseenter', '#jck_wt_slides', function(){
			$(this).find('.rslides_nav').fadeIn($fade_speed);
		}).on('mouseleave', '#jck_wt_slides', function(){
			$(this).find('.rslides_nav').fadeOut($fade_speed);
		});
	}

}); 

jQuery.fn.extend({
    unwrapInner: function(selector) {
        return this.each(function() {
            var t = this,
                c = jQuery(t).children(selector);
            if (c.length === 1) {
                c.contents().appendTo(t);
                c.remove();
            }
        });
    }
});