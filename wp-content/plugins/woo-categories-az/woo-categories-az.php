<?php
/**
* Plugin Name: Woocommerce Categories A-Z Filter
* Plugin URI: http://wordpress.org
* Description: Plugin to add alphabetical filter to Woocommerce categories
* Version: 1.0.1
* Author: Spyros Vlachopoulos
* Author URI: http://wordpress.org
* License: GPL2
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


function caz_create_categories_links() {
  
  global $wp_query;
  $cat_obj = $wp_query->get_queried_object();
  $parent_cat_id = caz_get_parent_terms($cat_obj);
  
  $args = array(
    'taxonomy'     => 'product_cat',
    'orderby'      => 'name',
    'show_count'   => 0,
    'pad_counts'   => 0,
    'hierarchical' => 1,
    'title_li'     => '',
    'parent'       => $parent_cat_id,
    'hide_empty'   => 1
  );
  $all_categories = get_categories( $args );
 
  $cat_array = array();
  if (!empty($all_categories)) {
    foreach ($all_categories as $cat) {
      
      $cat_name = $cat->name;
      
      if (get_option('woocommerce_filteraz_what', 'lastname') == 'lastname') {
        $cat_name = end(explode(' ', $cat_name));
      }
      $cat_array[] = strtoupper($cat_name[0]);
      
    }
    
    $cat_array = array_unique($cat_array);
    sort($cat_array);
  } else {
    return '';
  }
  
  $current_url = home_url( add_query_arg( null, null ));
  
  if (get_option('woocommerce_filteraz_all') == 'yes') {
    $links_array = array_combine(range('A','Z'),range('A','Z'));
  } else {
    $links_array = array();
  }
  
  if(!empty($cat_array)) {
    foreach ($cat_array as $letter) {
      
      $link = add_query_arg( array('filteraz' => $letter), $current_url);
      
      $links_array[$letter] = '<a class="'. (isset($_GET['filteraz']) && $_GET['filteraz'] == $letter ? 'selected':'') .'" href="'. $link .'" title="'. __('Filter by letter', 'caz') .' '. $letter .'"> '. $letter .'</a>';
    }
  }  
  
  return implode(' | ', $links_array);
  
}

function caz_get_parent_terms($term) {

  return $term->term_id;

}

// hook the links to the top of the description
add_action ('woocommerce_archive_description', 'caz_archive_description', 1);
function caz_archive_description() {
  

  $shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
  $current_url = reset((explode('?', home_url( add_query_arg( null, null ))))); // without query
  
  // if (!isset($post))
  
  $term = get_queried_object();
  if ( $term ) {
    $taxonomy = $term->taxonomy;
    $term_id  = $term->term_id;
  }
  
  $filteraz = get_term_meta( $term_id, 'filteraz', true );

  
  

  
  if ( $filteraz == 'yes' || (get_option('woocommerce_main_filteraz') == 'yes' && $shop_page_url == $current_url) ) {
    echo '<div class="filteraz">'. caz_create_categories_links() .' | <a href="'. $current_url .'" title="'. __('Reset', 'caz') .'">'. __('Reset', 'caz') .'</a></div>';
  }
}


add_action ('woocommerce_product_subcategories_args', 'caz_product_subcategories_args');

function caz_product_subcategories_args($args) {
  
  if (isset($_GET['filteraz']) && !empty($_GET['filteraz'])) {
    $args['firstname'] = strtoupper($_GET['filteraz']);
  }
  
  return $args;
}

function caz_get_terms_fields( $clauses, $taxonomies, $args ) {
    if ( ! empty( $args['firstname'] ) ) {
        global $wpdb;

        $firstname_like       = $wpdb->esc_like( $args['firstname'] );
        $firstname_like_lower = strtolower( $wpdb->esc_like( $args['firstname'] ) );

        if ( ! isset( $clauses['where'] ) ) {
          $clauses['where'] = '1=1';
        }
        
        
        if (get_option('woocommerce_filteraz_what', 'lastname') == 'firstname') {
          $clauses['where'] .= $wpdb->prepare( " AND (t.name LIKE %s OR t.name LIKE %s)", "$firstname_like%", "$firstname_like_lower" );  // OR t.name LIKE %s , "% $firstname_like%"
        }
        if (get_option('woocommerce_filteraz_what', 'lastname') == 'lastname') {
          $clauses['where'] .= $wpdb->prepare( " AND (
              (t.name LIKE %s AND t.name NOT LIKE %s) 
            OR 
              (t.name LIKE %s AND t.name NOT LIKE %s)
            OR 
              (t.name LIKE %s AND t.name NOT LIKE %s)
            OR
              (t.name LIKE %s AND t.name NOT LIKE %s)
          )", 
          "% $firstname_like%", "% $firstname_like% %", "$firstname_like%", "$firstname_like% %", 
          "% $firstname_like_lower%", "% $firstname_like_lower% %", "$firstname_like_lower%", "$firstname_like_lower% %" );  // OR t.name LIKE %s , "% $firstname_like%"
        }
    }
    
    // echo '<pre>'.print_r($clauses, true).'</pre>';

    return $clauses;
}

add_filter( 'terms_clauses', 'caz_get_terms_fields', 10, 3 );


// add field on woocommerce categories

// Add term page
function caz_taxonomy_add_new_meta_field($taxonomy) {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field term-group">
		<label for="filteraz"><?php _e( 'Show A-Z Filter', 'caz' ); ?></label>
		<select name="filteraz" id="filteraz">
      <option value="no"><?php echo __('No', 'caz'); ?></option>
      <option value="yes"><?php echo __('Yes', 'caz'); ?></option>
    </select>
	</div>
<?php
}
add_action( 'product_cat_add_form_fields', 'caz_taxonomy_add_new_meta_field', 10, 2 );

add_action( 'created_product_cat', 'caz_save_feature_meta', 10, 2 );

function caz_save_feature_meta( $term_id, $tt_id ){
    if( isset( $_POST['filteraz'] ) && '' !== $_POST['filteraz'] ){
        $group = sanitize_title( $_POST['filteraz'] );
        add_term_meta( $term_id, 'filteraz', $group, true );
    }
}

// Edit term page
function caz_taxonomy_edit_meta_field($term, $taxonomy) {
 
	// get current filter state
    $filteraz = get_term_meta( $term->term_id, 'filteraz', true );
                
    ?><tr class="form-field term-group-wrap">
        <th scope="row"><label for="filteraz"><?php _e( 'Show A-Z Filter', 'caz' ); ?></label></th>
        <td><select class="postform" id="filteraz" name="filteraz">
            <option value="no" <?php selected( $filteraz, 'no' ); ?>><?php echo __('No', 'caz'); ?></option>
            <option value="yes" <?php selected( $filteraz, 'yes' ); ?>><?php echo __('Yes', 'caz'); ?></option>
        </select></td>
    </tr>
<?php
}
add_action( 'product_cat_edit_form_fields', 'caz_taxonomy_edit_meta_field', 10, 2 );

add_action( 'edited_product_cat', 'caz_update_feature_meta', 10, 2 );

function caz_update_feature_meta( $term_id, $tt_id ){

    if( isset( $_POST['filteraz'] ) && '' !== $_POST['filteraz'] ){
        $group = sanitize_title( $_POST['filteraz'] );
        update_term_meta( $term_id, 'filteraz', $group );
    }
}

add_filter('manage_edit-product_cat_columns', 'caz_add_product_cat_column' );

function caz_add_product_cat_column( $columns ){
    
    $res = array_slice($columns, 0, 4, true) +
    array('filteraz' => __( 'Filter A-Z', 'caz' )) +
    array_slice($columns, 4, count($columns)-4, true);
    
    
    return $res;
}

add_filter('manage_product_cat_custom_column', 'caz_add_product_cat_column_content', 10, 3 );

function caz_add_product_cat_column_content( $content, $column_name, $term_id ){

    if( $column_name !== 'filteraz' ){
        return $content;
    }

    $term_id = absint( $term_id );
    $filteraz = get_term_meta( $term_id, 'filteraz', true );

    if( !empty( $filteraz ) ){
        $content = esc_attr( $filteraz );
    }

    return $content;
}

// add woocommerce catalog settings
add_filter( 'woocommerce_general_settings', 'caz_add_main_filteraz' );
function caz_add_main_filteraz($settings) {
  
  $updated_settings = array();
  
  foreach ( $settings as $section ) {
    
    if ( isset( $section['id'] ) && 'general_options' == $section['id'] && isset( $section['type'] ) && 'sectionend' == $section['type'] ) {
  
      $updated_settings[] = array(
          'name'    => __( 'A-Z Filter on main Shop page', 'woocommerce' ),
          'id'      => 'woocommerce_main_filteraz',
          'css'     => 'min-width:150px;',
          'std'     => 'no', // WooCommerce < 2.0
          'default' => 'no', // WooCommerce >= 2.0
          'type'    => 'select',
          'options' => array(
            'no'        => __( 'No', 'caz' ),
            'yes'       => __( 'Yes', 'caz' ),
          ),
          'desc_tip' =>  false,

        );
        $updated_settings[] = array(
          'name'    => __( 'A-Z Filter display all letters', 'woocommerce' ),
          'id'      => 'woocommerce_filteraz_all',
          'css'     => 'min-width:150px;',
          'std'     => 'no', // WooCommerce < 2.0
          'default' => 'no', // WooCommerce >= 2.0
          'type'    => 'select',
          'options' => array(
            'no'        => __( 'No', 'caz' ),
            'yes'       => __( 'Yes', 'caz' ),
          ),
          'desc_tip' =>  false,

        );
        $updated_settings[] = array(
          'name'    => __( 'Filter A-Z by', 'woocommerce' ),
          'id'      => 'woocommerce_filteraz_what',
          'css'     => 'min-width:150px;',
          'std'     => 'lastname', // WooCommerce < 2.0
          'default' => 'lastname', // WooCommerce >= 2.0
          'type'    => 'select',
          'options' => array(
            'lastname'        => __( 'Last Name', 'caz' ),
            'firstname'       => __( 'First Name', 'caz' ),
          ),
          'desc_tip' =>  false,

        );
    }
    $updated_settings[] = $section;
  }
    
    
    

  return $updated_settings;
  
}
?>