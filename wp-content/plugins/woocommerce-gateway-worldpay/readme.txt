1 - Install the plugin
2 - activate the plugin
3 - go to settings
4 - enable
5 - create a Callback password
6 - add an MD5 Secret
7 - save the settings
8 - login to WorldPay
9 - find your test installation
10- copy your callback password, MD5 secret and Payment Response URL in to the WorldPay settings
11- Set the
11- save the settings
12- place test orders in your shop ( test card 4111 1111 1111 1111, exp 12/16, CVV 123 )
13- Once you are satisfied everything is working you can change the Status to live then login in to WorldPay and copy the test installation to the Production Installation.

Subscription Cancellations and Refunds. 

For subscription cancellations and refunds to work you will need a separate Remote Admin Installation ID and Password. WorldPay will only provide this if you contact them and ask for it.

Once you have the Remote Admin Installation ID and Password you need to enter them in to your WooCommerce WorldPay settings and save them. You do not need to make any changes in your test or live WorldPay Installations











How to setup your WorldPay "Installation Administration"

After you have logged into WorldPay click on 'Installations' in the left menu

Now click on the spanner/wrench under 'Integration Setup : TEST' for your 'Select Junior' account

You need to edit the following fields as per the image 'WorldPay-Settings-ScreenShot.jpg', included with this plugin.

'Store-builder used' change to 'Other'
'store-builder: if other - please specify' set as 'WooCommerce'
'Payment Response URL' needs to be EITHER

http://<wpdisplay item="MC_callback">

OR

https://<wpdisplay item="MC_callback">

depending on whether you use http or https for your site

'Payment Response enabled?', 'Enable Recurring Payment Response' and 'Enable the Shopper Response ' all need to be ticked


'Payment Response password' needs to be set

Click 'Save Changes' and then 'OK'


You need to copy the 'Payment Response password' and 'Installation ID' into the 
settings panel of the WorldPay payment gateway in your WooCommerce admin