<?php
/**
 * Default Content Template
 *
 * This template is the default content template. It is used to display the content of a
 * template file, when no more specific content-*.php file is available.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */
 global $woo_options;
 $title_before = '<h1 class="title">';
 $title_after = '</h1>';

 $page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

 woo_post_before();
?>
<article <?php post_class(); ?>>
<?php
	woo_post_inside_before();
?>

<div id="homeslider">
  
  	<section class="entry">
  	<div id="hometext">
	    <?php
	    	the_content();
	    	if ( $woo_options['woo_post_content'] == 'content' || is_singular() ) wp_link_pages( $page_link_args );
	    ?>
	  </div>
	</section><!-- /.entry -->
  
  <?php if ( function_exists( 'soliloquy_slider' ) ) soliloquy_slider( '8' ); ?>
</div>

  	<div id="hometextmobile">
	    <?php
	    	the_content();
	    	if ( $woo_options['woo_post_content'] == 'content' || is_singular() ) wp_link_pages( $page_link_args );
	    ?>
	  </div>


<div id="homehotlinks">
    <div class="homefeature" id="homefeatureleft">
      <a href="<?php the_field('featured_print_link'); ?>">
        <h2>Featured Prints</h2>
<?php
$attachment_id = get_field('featured_prints');
$size = "medium"; // (thumbnail, medium, large, full or custom size)

$image = wp_get_attachment_image_src( $attachment_id, $size );
// url = $image[0];
// width = $image[1];
// height = $image[2];
?>
<img src="<?php echo $image[0]; ?>" />    
      </a>
    </div>
    <div class="homefeature">
<a href="<?php the_field('whats_new_link'); ?>">
        <h2>What's new</h2>
<?php
$attachment_id = get_field('whats_new');
$size = "medium"; // (thumbnail, medium, large, full or custom size)

$image = wp_get_attachment_image_src( $attachment_id, $size );
// url = $image[0];
// width = $image[1];
// height = $image[2];
?>
<img src="<?php echo $image[0]; ?>" />    
      </a>      
    </div>
</div>




	</section><!-- /.entry -->
	<div class="fix"></div>
<?php
	woo_post_inside_after();
?>
</article><!-- /.post -->
<?php
	woo_post_after();
/*
	$comm = $woo_options[ 'woo_comments' ];
	if ( ( $comm == 'page' || $comm == 'both' ) && is_page() ) { comments_template(); }
*/
?>