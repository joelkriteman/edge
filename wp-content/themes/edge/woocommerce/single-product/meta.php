<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
	
		<h3 class="papertitle">Paper Size Guide</h3>
	
	<table id="productsizes">
			<tr>
					<td class="sizesize">Paper Size*<br /><span class="inches">(inches)</span> / (cm)</td>
					<td class="sizesize">Landscape or Portrait images<br />
					(horizontal or vertical)</td>
					<td class="sizesize">Square images</td>
			</tr>
			
			<tr>
					<td>Regular</td>
					<td><span class="inches">10 x 12</span><br />25 x 30</td>
						<td><span class="inches">10 x 10</span><br />25 x 25</td>
			</tr>
			
			<tr>
					<td>Classical</td>
					<td><span class="inches">12 x 16</span><br />30 x 40</td>
					<td><span class="inches">12 x 12</span><br />30 x 30</td>
			</tr>

			<tr>
					<td>Luxe</td>
					<td><span class="inches">16 x 20</span><br />40 x 51</td>
					<td><span class="inches">16 x 16</span><br />40 x 40</td>
			</tr>
			
			
			<tr>
					<td>Large</td>
					<td><span class="inches">20 x 24</span><br />51 x 61</td>
					<td><span class="inches">20 x 20</span><br />51 x 51</td>
			</tr>
			
			
			<tr>
					<td>Longe</td>
					<td><span class="inches">20 x 30</span><br />51 x 76</td>
					<td>n/a</td>
			</tr>
			
			<tr>
					<td>Grande XL</td>
					<td><span class="inches">30 x 40</span><br />76 x 101</td>
					<td><span class="inches">30 x 30</span><br />76 x 76</td>
			</tr>

			<tr>
					<td>Giant</td>
					<td><span class="inches">40 x 60</span><br />101 x 152</td>
					<td><span class="inches">40 x 40</span><br />101 x 101</td>	
			</tr>
	
	</table>
	
	<p class="paperguide">*Note that the resulting image size can differ from the paper size it is printed on, as it will be the maximum fit of the image onto your chosen paper size including a minimum small white border around the image.</p>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
