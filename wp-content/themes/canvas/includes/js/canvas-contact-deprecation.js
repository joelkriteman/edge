/*-----------------------------------------------------------------------------------*/
/* Handles deprecation notice dismissal for Canvas Contact Form Page Template */
/*-----------------------------------------------------------------------------------*/
jQuery(function($) {
    $( document ).on( 'click', '.canvas-contact-deprecation .notice-dismiss', function () {
        var type = $( this ).closest( '.canvas-contact-deprecation' ).data( 'notice' );
        $.ajax( ajaxurl,
            {
              type: 'POST',
              data: {
                action: 'canvas_dismissed_notice_handler',
                type: type,
              }
            } );
      } );
  });